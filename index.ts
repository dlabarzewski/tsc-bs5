import { Modal } from 'bootstrap';

window.addEventListener('load', e => {
    const element = document.getElementById('myModal') as HTMLElement;
    const myModal = new Modal(element);

    const button = document.querySelector('#button');

    if (button)
    {
        button.addEventListener('click', e => {
            myModal.show();
        });
    }
});